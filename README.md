# Sample test of automation for create a alpine lxc from official image and install and run docker service

## what do you need?

- proxmox VE installation
- internet connection
- a LXC/VM to use like infrastructure control center [ICC]
- ansible installed [icc]
- terraform installed [icc]

## what do I get at the end?

- 1 alpine LXC with docker environment ready

## what steps are necessary?

- upload on a pve node a official LXC alpine template
- create a LXC from the template and adding the basic service/pkgs (ssh server and python 3 interpreter)
- creating a new template from the new LXC custom image
- create and modify a vars.tf files
- use terraform for create 1 LXC from the modified template
- execute ansible playbook on this LXC for create a docker environment

## how to get a alpine LXC template?

- `pveam update`
- `pveam available`
- `pveam download $storage_template $template_name`

## how setup ssh server and python for ansible connection on new alpine LXC
- `pct create <$tmp_id $full/path/to/template/folder + / + $default_template.tar.gz> -arch amd64 -ostype alpine -net0 name=eth0,bridge= $netbridge ,gw= $gateway ,ip= &ip / $netmask ,type=veth  -storage $storage_for_lxc`
- `pct start $tmp_id`
- `pct exec $tmp_id -- sh -c "apk update && apk upgrade && apk add openssh python3 && rc-update add sshd && service sshd start"`
- `pct exec $tmp_id -- sh -c "ip link delete eth0 && rm /etc/hostname"`
- `pct stop $tmp_id`
- `vzdump $tmp_id --dumpdir $full/path/to/template/folder --compress gzip`

## how to use terraform?

- `terraform -chdir=terraform init`
- `terraform -chdir=terraform plan`
- `terraform -chdir=terraform apply`

## how to use ansible?

- `ansible-playbook -i ansible/ansible_hosts.txt  ansible/ansible_lxc_add_docker.yaml`
