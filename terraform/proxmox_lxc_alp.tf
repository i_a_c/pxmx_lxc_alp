terraform {
  required_providers {
    proxmox         = {
      source        = "telmate/proxmox"
      version       = "2.9.10"
    }
  }
}

resource "proxmox_lxc" "test-lxc-alpine" {
  target_node       = var.proxmox_host
  hostname          = var.hostname
  cores             = var.cores
  memory            = var.memory
  swap              = var.swap
  ostemplate        = format("%s/%s", var.template_storage,var.template_name)
  unprivileged      = true
  ostype            = "alpine"
  vmid              = var.id
  password          = var.rootpassword

  features {
    nesting         = true
  }

  rootfs {
    storage         = var.storage_device
    size            = var.storage_size
  }

  network {
    name            = "eth0"
    bridge          = var.bridge_net
    ip              = format("%s/%s", var.ip, var.nm)
    gw              = var.gw
    firewall        = false
  }
  
  nameserver        = var.dns
  searchdomain      = var.domain
  ssh_public_keys   = var.ssh_key
}
